package pilkowskikulpan.spagheapi.src.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.recipe.Recipe;

public class YourRecipeListAdapter extends RecyclerView.Adapter<YourRecipeListAdapter.RecipeListViewHolder> {
    private ArrayList<Recipe> recipesList;
    private OnItemClickListener mListener;
    public interface OnItemClickListener{
        void onEdited(int position);

        void onDeleted(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }
    public YourRecipeListAdapter(ArrayList<Recipe> recipesList) {
        this.recipesList = recipesList;
    }
    @NonNull
    @Override
    public RecipeListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.your_single_recipe, parent, false);
        return new RecipeListViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeListViewHolder holder, int position) {
        holder.mName.setText(recipesList.get(position).getName());
        holder.mIngredients.setText(recipesList.get(position).getIngredients());
        holder.mPreperation.setText(recipesList.get(position).getPreperation());
    }

    @Override
    public int getItemCount() {
        if (recipesList instanceof ArrayList) {
            return recipesList.size();
        }
        return 0;
    }

    class RecipeListViewHolder extends RecyclerView.ViewHolder {
        TextView mName, mIngredients, mPreperation;
        ImageView mDelete,mEdit;
        RecipeListViewHolder(View view, final OnItemClickListener listener) {
            super(view);
            mName = view.findViewById(R.id.recipe_management_name);
            mIngredients = view.findViewById(R.id.recipe_management_ingredients);
            mPreperation = view.findViewById(R.id.recipe_management_preperation);
            mDelete=view.findViewById(R.id.recipe_management_delete);
            mEdit=view.findViewById(R.id.recipe_management_edit);
            mDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onDeleted(position);
                        }
                    }
                }
            });
            mEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onEdited(position);
                        }
                    }
                }
            });
        }

    }
}

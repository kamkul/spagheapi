package pilkowskikulpan.spagheapi.src.adapters;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;

public class DetailedRecipePhotosListAdapter extends RecyclerView.Adapter<DetailedRecipePhotosListAdapter.DetailedRecipePhotosViewHolder> {
    private ArrayList<Bitmap> recipePhotos;
    private OnItemClickListener mListener;

    public DetailedRecipePhotosListAdapter(ArrayList<Bitmap> recipePhotos) {
        this.recipePhotos = recipePhotos;
    }

    public void setOnItemClickListener(DetailedRecipePhotosListAdapter.OnItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public DetailedRecipePhotosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.detailed_recipe_photo, parent, false);
        return new DetailedRecipePhotosListAdapter.DetailedRecipePhotosViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailedRecipePhotosViewHolder holder, int position) {
        if (recipePhotos != null)
            holder.recipePhoto.setImageBitmap(recipePhotos.get(position));
    }

    @Override
    public int getItemCount() {
        if (recipePhotos instanceof ArrayList) {
            return recipePhotos.size();
        }
        return 0;
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }

    public class DetailedRecipePhotosViewHolder extends RecyclerView.ViewHolder {

        ImageView recipePhoto;

        public DetailedRecipePhotosViewHolder(View view, final OnItemClickListener listener) {
            super(view);
            recipePhoto = view.findViewById(R.id.recipe_photo);
        }
    }
}


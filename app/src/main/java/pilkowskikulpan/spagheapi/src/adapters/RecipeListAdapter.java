package pilkowskikulpan.spagheapi.src.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.activities.DetailedRecipeActivity;
import pilkowskikulpan.spagheapi.src.favourites.DaggerFavouritesComponent;
import pilkowskikulpan.spagheapi.src.favourites.FavouritesService;
import pilkowskikulpan.spagheapi.src.favourites.IsRecipeFavouriteQuery;
import pilkowskikulpan.spagheapi.src.recipe.Recipe;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.DaggerUserComponent;
import pilkowskikulpan.spagheapi.src.user.GetCreatorNameQuery;
import pilkowskikulpan.spagheapi.src.user.UserService;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeListViewHolder> {
    private ArrayList<Recipe> recipesList;
    private OnItemClickListener mListener;
    private UserService userService;
    private FavouritesService favouritesService;
    public RecipeListAdapter(ArrayList<Recipe> recipesList) {
        this.userService = DaggerUserComponent.create().getService();
        this.favouritesService = DaggerFavouritesComponent.create().getService();
        this.recipesList = recipesList;
    }

    public void setOnItemClickListener(RecipeListAdapter.OnItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public RecipeListAdapter.RecipeListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_recipe, parent, false);
        return new RecipeListAdapter.RecipeListViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecipeListViewHolder holder, final int position) {
        holder.mName.setText(recipesList.get(position).getName());
        holder.mIngredients.setText(recipesList.get(position).getIngredients());
        holder.mPreperation.setText(recipesList.get(position).getPreperation());
        GetCreatorNameQuery getCreatorNameQuery = new GetCreatorNameQuery();
        getCreatorNameQuery.setRecipeUid(recipesList.get(position).getuId());
        this.userService.getCreatorName(getCreatorNameQuery, new AsyncResponse<String>() {
            @Override
            public void onSuccess(String s) {
                holder.mCreator.setText(new StringBuilder("Dodano przez: " + s));
            }

            @Override
            public void onException(Exception e) {

            }
        });
        IsRecipeFavouriteQuery isRecipeFavouriteQuery = new IsRecipeFavouriteQuery();
        isRecipeFavouriteQuery.setRecipeUid(recipesList.get(position).getuId());
        favouritesService.isFavourite(isRecipeFavouriteQuery, new AsyncResponse<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (aBoolean) holder.mFavourite.setImageResource(R.drawable.ic_favourite_red_24dp);
                else
                    holder.mFavourite.setImageResource(R.drawable.ic_no_favourite_24dp);
            }

            @Override
            public void onException(Exception e) {
            }
        });
    }

    @Override
    public int getItemCount() {
        if (recipesList instanceof ArrayList) {
            return recipesList.size();
        }
        return 0;
    }

    public interface OnItemClickListener {
        void onAdded(int position);

        void onDeleted(int position);
    }

    class RecipeListViewHolder extends RecyclerView.ViewHolder {
        TextView mName, mIngredients, mPreperation, mCreator;
        ImageView mFavourite;
        RecipeListViewHolder(View view, final OnItemClickListener listener) {
            super(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), DetailedRecipeActivity.class);
                    int position = getAdapterPosition();
                    intent.putExtra("recipeName", recipesList.get(position).getName());
                    intent.putExtra("recipeIngredients", recipesList.get(position).getIngredients());
                    intent.putExtra("recipePreparation", recipesList.get(position).getPreperation());
                    intent.putExtra("recipeID", recipesList.get(position).getuId());
                    intent.putExtra("recipePhotosIDs", recipesList.get(position).getPhotoIDs());
                    view.getContext().startActivity(intent);
                }
            });
            mName = view.findViewById(R.id.mainpage_recipe_name);
            mIngredients = view.findViewById(R.id.mainpage_recipe_ingredients);
            mPreperation = view.findViewById(R.id.mainpage_recipe_preperation);
            mCreator = view.findViewById(R.id.mainpage_recipe_creator);
            mFavourite = view.findViewById(R.id.mainpage_favourite);
            mFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (mFavourite.getDrawable().getConstantState().equals(mFavourite.getResources()
                            .getDrawable(R.drawable.ic_favourite_red_24dp).getConstantState()))
                        if (listener != null)
                            if (position != RecyclerView.NO_POSITION) listener.onDeleted(position);
                    if (mFavourite.getDrawable().getConstantState().equals(mFavourite.getResources()
                            .getDrawable(R.drawable.ic_no_favourite_24dp).getConstantState()))
                        if (listener != null)
                            if (position != RecyclerView.NO_POSITION) listener.onAdded(position);
                }
            });
        }

    }
}

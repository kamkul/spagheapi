package pilkowskikulpan.spagheapi.src.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.activities.DetailedRecipeActivity;
import pilkowskikulpan.spagheapi.src.recipe.Recipe;

public class FavouriteListAdapter extends RecyclerView.Adapter<FavouriteListAdapter.FavouriteListViewHolder> {
    private ArrayList<Recipe> recipesList;
    private OnItemClickListener mListener;

    public FavouriteListAdapter(ArrayList<Recipe> recipesList) {
        this.recipesList = recipesList;
    }

    public void setOnItemClickListener(FavouriteListAdapter.OnItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public FavouriteListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.favourite_recipe, parent, false);
        return new FavouriteListAdapter.FavouriteListViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouriteListViewHolder holder, int position) {
        holder.mName.setText(recipesList.get(position).getName());
        holder.mIngredients.setText(recipesList.get(position).getIngredients());
        holder.mPreperation.setText(recipesList.get(position).getPreperation());
    }

    @Override
    public int getItemCount() {
        if (recipesList instanceof ArrayList) {
            return recipesList.size();
        }
        return 0;
    }

    public interface OnItemClickListener {
        void onDeleted(int position);
    }

    class FavouriteListViewHolder extends RecyclerView.ViewHolder {
        TextView mName, mIngredients, mPreperation;
        ImageView mFavourtie;

        FavouriteListViewHolder(View view, final OnItemClickListener listener) {
            super(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), DetailedRecipeActivity.class);
                    int position = getAdapterPosition();
                    intent.putExtra("recipeName", recipesList.get(position).getName());
                    intent.putExtra("recipeIngredients", recipesList.get(position).getIngredients());
                    intent.putExtra("recipePreparation", recipesList.get(position).getPreperation());
                    intent.putExtra("recipeID", recipesList.get(position).getuId());
                    intent.putExtra("recipePhotosIDs", recipesList.get(position).getPhotoIDs());
                    view.getContext().startActivity(intent);
                }
            });
            mName = view.findViewById(R.id.mainpage_recipe_name);
            mIngredients = view.findViewById(R.id.mainpage_recipe_ingredients);
            mPreperation = view.findViewById(R.id.mainpage_recipe_preperation);

            mFavourtie = view.findViewById(R.id.mainpage_favourite);
            mFavourtie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) listener.onDeleted(position);
                    }
                }
            });
        }
    }
}

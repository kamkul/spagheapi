package pilkowskikulpan.spagheapi.src.user;

import dagger.Component;

@Component
public interface UserComponent {
    UserRepository getRepository();

    UserService getService();
}

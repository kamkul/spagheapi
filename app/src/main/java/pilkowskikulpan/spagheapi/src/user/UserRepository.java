package pilkowskikulpan.spagheapi.src.user;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import dagger.Module;

@Module
class UserRepository {

    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;

    @Inject
    UserRepository() {
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.firebaseStorage = FirebaseStorage.getInstance();
    }

    void register(final RegisterUserCommand registerUserCommand,
                  final AsyncResponse<FirebaseUser> asyncResponse) {

        firebaseAuth.createUserWithEmailAndPassword(
                registerUserCommand.getEmail(),
                registerUserCommand.getPassword())
                .addOnSuccessListener(
                        new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(final AuthResult authResult) {
                                User user = new User();
                                FirebaseUser firebaseUser = authResult.getUser();
                                DatabaseReference mUserDB;
                                mUserDB = firebaseDatabase.getReference().child("users")
                                        .child(firebaseUser.getUid());
                                user.setName(registerUserCommand.getName());
                                user.setEmail(registerUserCommand.getEmail());
                                mUserDB.setValue(user);
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(registerUserCommand.getName())
                                        .build();
                                firebaseUser.updateProfile(profileUpdates)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                asyncResponse.onSuccess(authResult.getUser());
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        asyncResponse.onException(e);
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                asyncResponse.onException(e);
            }
        });

        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser != null)
            firebaseUser.sendEmailVerification()
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            asyncResponse.onException(e);
                        }
                    });
    }

    void forgotPassword(ForgotPasswordQuery forgotPasswordQuery,
                        final AsyncResponse<Void> asyncResponse) {

        String email = forgotPasswordQuery.getEmail();
        firebaseAuth.sendPasswordResetEmail(email).
                addOnSuccessListener(new OnSuccessListener<Void>() {

                    @Override
                    public void onSuccess(Void aVoid) {
                        asyncResponse.onSuccess(aVoid);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                asyncResponse.onException(e);
            }
        });
    }

    void login(final LoginUserQuery loginUserQuery,
               final AsyncResponse<FirebaseUser> asyncResponse) {
        firebaseAuth.signInWithEmailAndPassword(
                loginUserQuery.getEmail(),
                loginUserQuery.getPassword())
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        System.out.println(authResult.getUser().describeContents());
                        asyncResponse.onSuccess(firebaseAuth.getCurrentUser());
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                asyncResponse.onException(e);
            }
        });
    }

    void logout(AsyncResponse<Void> asyncResponse) {
        firebaseAuth.signOut();
        asyncResponse.onSuccess(null);
        asyncResponse.onException(null);
    }

    FirebaseUser getCurrentUser() throws UserNotLoggedInException {
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser == null || firebaseUser.isAnonymous())
            throw new UserNotLoggedInException();
        return firebaseUser;
    }

    void getUserPhotoFromStorage(GetUserPhotoFromStorageQuery getUserPhotoFromStorageQuery, final AsyncResponse<Bitmap> asyncResponse) {
        try {
            final File localFile = File.createTempFile("profile_image", "jpg");

            firebaseStorage.getReference()
                    .child("users/" + getUserPhotoFromStorageQuery.getUser().getUid() + "/profilePicture/")
                    .getFile(localFile)
                    .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                            asyncResponse.onSuccess(bitmap);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    asyncResponse.onException(exception);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void setUserPhoto(final SetUserPhotoCommand setUserPhotoCommand, final AsyncResponse<Void> asyncResponse) {
        final StorageReference storageReference = firebaseStorage.getReference()
                .child("users/" + setUserPhotoCommand.getUser().getUid() + "/profilePicture/");
        storageReference.putFile(setUserPhotoCommand.getPath()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                storageReference.getDownloadUrl()
                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setPhotoUri(uri)
                                        .build();

                                setUserPhotoCommand.getUser()
                                        .updateProfile(profileUpdates)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                asyncResponse.onSuccess(aVoid);
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        asyncResponse.onException(e);
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        asyncResponse.onException(e);
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                asyncResponse.onException(e);
            }
        });
    }

    void setUserPhoto(SetUserEmailCommand setUserEmailCommand, final AsyncResponse<Void> asyncResponse) {
        setUserEmailCommand.getUser()
                .updateEmail(setUserEmailCommand.getEmail())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        asyncResponse.onSuccess(aVoid);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                asyncResponse.onException(e);
            }
        });
    }

    void setCurrentUserName(SetUserNameCommand setUserNameCommand, final AsyncResponse<Void> asyncResponse) {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(setUserNameCommand.getDisplayName())
                .build();

        setUserNameCommand.getUser()
                .updateProfile(profileUpdates)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        asyncResponse.onSuccess(aVoid);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                asyncResponse.onException(e);
            }
        });
    }

    void setCurrentUserPassword(final SetPasswordQuery setPasswordQuery, final AsyncResponse<Void> asyncResponse) {
        setPasswordQuery.getUser().updatePassword(setPasswordQuery.getPassword()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                asyncResponse.onSuccess(aVoid);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                asyncResponse.onException(e);
            }
        });
    }

    void getCreatorName(final GetCreatorNameQuery getCreatorNameQuery, final AsyncResponse<String> asyncResponse) {
        final String[] creator = new String[1];
        firebaseDatabase.getReference().child("recipes").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                creator[0] = dataSnapshot.child(getCreatorNameQuery.getRecipeUid()).child("creator").getValue().toString();
                firebaseDatabase.getReference().child("users").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            asyncResponse.onSuccess(dataSnapshot.child(creator[0]).child("name").getValue().toString());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        asyncResponse.onException(databaseError.toException());
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                asyncResponse.onException(databaseError.toException());
            }
        });
    }
}

package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

import com.google.firebase.auth.FirebaseUser;

public class SetPasswordQuery {
    @Nullable
    private FirebaseUser user;
    @Nullable
    private String password;

    @Nullable
    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(@Nullable FirebaseUser user) {
        this.user = user;
    }

    @Nullable
    String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }
}

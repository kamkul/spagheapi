package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

public class LoginUserQuery {
    @Nullable
    private String email;
    @Nullable
    private String password;

    @Nullable
    String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    @Nullable
    String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    public void isValid() throws EmailNotValidException, PasswordNotValidException {
        if (email == null || email.isEmpty())
            throw new EmailNotValidException();
        if (password == null || password.isEmpty())
            throw new PasswordNotValidException();
    }

    public class EmailNotValidException extends Exception {

    }

    public class PasswordNotValidException extends Exception {

    }
}

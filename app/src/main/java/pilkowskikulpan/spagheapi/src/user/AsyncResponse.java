package pilkowskikulpan.spagheapi.src.user;

public interface AsyncResponse <T> {
    void onSuccess(T t);
    void onException(Exception e);
}

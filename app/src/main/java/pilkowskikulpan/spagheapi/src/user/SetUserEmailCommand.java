package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

import com.google.firebase.auth.FirebaseUser;

public class SetUserEmailCommand {
    @Nullable
    private FirebaseUser user;
    @Nullable
    private String email;

    @Nullable
    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(@Nullable FirebaseUser user) {
        this.user = user;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    public void isValid() {

    }


}

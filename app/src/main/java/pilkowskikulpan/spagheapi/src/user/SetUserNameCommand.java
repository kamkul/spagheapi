package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

import com.google.firebase.auth.FirebaseUser;

public class SetUserNameCommand {
    @Nullable
    private FirebaseUser user;
    @Nullable
    private String displayName;

    @Nullable
    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(@Nullable FirebaseUser user) {
        this.user = user;
    }

    @Nullable
    String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(@Nullable String displayName) {
        this.displayName = displayName;
    }

    public void isValid() {

    }


}

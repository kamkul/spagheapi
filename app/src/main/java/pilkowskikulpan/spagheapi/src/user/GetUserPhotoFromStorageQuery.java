package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

import com.google.firebase.auth.FirebaseUser;

public class GetUserPhotoFromStorageQuery {

    @Nullable
    private FirebaseUser user;

    @Nullable
    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(@Nullable FirebaseUser user) {
        this.user = user;
    }

    public void isValid() {
    }
}

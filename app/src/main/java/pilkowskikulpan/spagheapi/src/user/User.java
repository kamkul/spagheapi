package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

public class User {
    @Nullable
    private String name;
    @Nullable
    private String email;

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }
}

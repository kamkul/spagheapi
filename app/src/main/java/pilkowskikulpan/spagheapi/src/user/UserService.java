package pilkowskikulpan.spagheapi.src.user;

import android.graphics.Bitmap;

import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

public class UserService {

    private final UserRepository userRepository;

    @Inject
    UserService() {
        UserComponent recipeComponent = DaggerUserComponent.create();
        this.userRepository = recipeComponent.getRepository();
    }

    public void forgotPassword(ForgotPasswordQuery forgotPasswordQuery,
                               AsyncResponse<Void> asyncResponse)
            throws ForgotPasswordQuery.EmailNotValidException {

        forgotPasswordQuery.isValid();
        this.userRepository.forgotPassword(forgotPasswordQuery, asyncResponse);
    }

    public void register(RegisterUserCommand registerUserCommand,
                         AsyncResponse<FirebaseUser> asyncResponse)
            throws RegisterUserCommand.EmailNotValidException, RegisterUserCommand.PasswordNotValidException {

        registerUserCommand.isValid();
        this.userRepository.register(registerUserCommand, asyncResponse);
    }

    public void login(LoginUserQuery loginUserQuery, AsyncResponse<FirebaseUser> asyncResponse)
            throws LoginUserQuery.EmailNotValidException, LoginUserQuery.PasswordNotValidException {
        loginUserQuery.isValid();
        this.userRepository.login(loginUserQuery, asyncResponse);
    }

    public void logout(AsyncResponse<Void> asyncResponse) {
        this.userRepository.logout(asyncResponse);
    }

    public FirebaseUser getCurrentUser() throws UserNotLoggedInException {
        return this.userRepository.getCurrentUser();
    }

    public void getUserPhotoFromStorage(GetUserPhotoFromStorageQuery getUserPhotoFromStorageQuery, AsyncResponse<Bitmap> asyncResponse) {
        this.userRepository.getUserPhotoFromStorage(getUserPhotoFromStorageQuery, asyncResponse);
    }

    public void setUserPhoto(SetUserPhotoCommand setUserPhotoCommand, AsyncResponse<Void> asyncResponse) {
        this.userRepository.setUserPhoto(setUserPhotoCommand, asyncResponse);
    }

    public void setUserEmail(SetUserEmailCommand setUserEmailCommand, AsyncResponse<Void> asyncResponse) {
        this.userRepository.setUserPhoto(setUserEmailCommand, asyncResponse);
    }

    public void setUserName(SetUserNameCommand setUserNameCommand, AsyncResponse<Void> asyncResponse) {
        this.userRepository.setCurrentUserName(setUserNameCommand, asyncResponse);
    }

    public void setUserPassword(SetPasswordQuery setPasswordQuery, AsyncResponse<Void> asyncResponse) {
        this.userRepository.setCurrentUserPassword(setPasswordQuery, asyncResponse);
    }

    public void getCreatorName(GetCreatorNameQuery getCreatorNameQuery, AsyncResponse<String> asyncResponse) {
        this.userRepository.getCreatorName(getCreatorNameQuery, asyncResponse);
    }
}

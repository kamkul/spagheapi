package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

public class ForgotPasswordQuery {

    @Nullable
    private String email;

    @Nullable
    String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    void isValid() throws EmailNotValidException {
        if (email == null || email.isEmpty())
            throw new EmailNotValidException();
    }

    public class EmailNotValidException extends Exception {

    }
}

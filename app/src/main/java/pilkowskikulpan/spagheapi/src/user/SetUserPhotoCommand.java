package pilkowskikulpan.spagheapi.src.user;

import android.net.Uri;

import androidx.annotation.Nullable;

import com.google.firebase.auth.FirebaseUser;

public class SetUserPhotoCommand {
    @Nullable
    private FirebaseUser user;
    @Nullable
    private Uri path;

    @Nullable
    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(@Nullable FirebaseUser user) {
        this.user = user;
    }

    @Nullable
    public Uri getPath() {
        return path;
    }

    public void setPath(@Nullable Uri path) {
        this.path = path;
    }

    public boolean isValid() {
        return false;
    }
}

package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

public class RegisterUserCommand {
    @Nullable
    private String email;
    @Nullable
    private String password;
    @Nullable
    private String name;

    void isValid() throws EmailNotValidException, PasswordNotValidException {
        if (email == null || email.isEmpty())
            throw new EmailNotValidException();
        if (password == null || password.isEmpty())
            throw new PasswordNotValidException();
    }

    @Nullable
    String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    @Nullable
    String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    public class EmailNotValidException extends Exception {

    }

    public class PasswordNotValidException extends Exception {

    }
}

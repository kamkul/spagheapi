package pilkowskikulpan.spagheapi.src.user;

import androidx.annotation.Nullable;

public class GetCreatorNameQuery {
    @Nullable
    private String recipeUid;

    @Nullable
    public String getRecipeUid() {
        return recipeUid;
    }

    public void setRecipeUid(@Nullable String recipeUid) {
        this.recipeUid = recipeUid;
    }

    public boolean isValid() {
        return false;
    }

}

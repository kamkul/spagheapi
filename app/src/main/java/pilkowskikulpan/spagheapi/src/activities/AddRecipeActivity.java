package pilkowskikulpan.spagheapi.src.activities;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.recipe.AddRecipeCommand;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.UserNotLoggedInException;

public class AddRecipeActivity extends CommonActivity {

    static final int RESULT_LOAD_IMAGE = 1;
    ArrayList<Uri> loadedImages = new ArrayList<>();
    private TextView recipeNameTextView, ingredientsTextView, preparationTextView;
    private Button addRecipeButton, addRecipePhotoButton;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Uri selectedImage = data.getData();
            loadedImages.add(selectedImage);
            Toast.makeText(this, "Dodano zdjęcie", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);
        recipeNameTextView = findViewById(R.id.recipeName);
        ingredientsTextView = findViewById(R.id.ingredients);
        preparationTextView = findViewById(R.id.preparation);
        addRecipeButton = findViewById(R.id.addRecipe);
        addRecipePhotoButton = findViewById(R.id.addRecipePhoto);

        addRecipePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        addRecipeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AddRecipeCommand addRecipeCommand = new AddRecipeCommand();
                    addRecipeCommand.setName(recipeNameTextView.getText().toString());
                    addRecipeCommand.setIngredients(ingredientsTextView.getText().toString());
                    addRecipeCommand.setPreperation(preparationTextView.getText().toString());
                    addRecipeCommand.setCreator(userService.getCurrentUser().getUid());
                    addRecipeCommand.setPhotoUris(loadedImages);
                    recipeService.addRecipe(addRecipeCommand, new AsyncResponse<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Dodano przepis!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        }

                        @Override
                        public void onException(Exception exception) {
                            Toast.makeText(getApplicationContext(), "Coś poszło nie tak", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (UserNotLoggedInException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}

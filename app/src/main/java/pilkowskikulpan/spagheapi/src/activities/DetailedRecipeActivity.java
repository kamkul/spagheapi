package pilkowskikulpan.spagheapi.src.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.adapters.DetailedRecipePhotosListAdapter;
import pilkowskikulpan.spagheapi.src.recipe.GetRecipePhotosQuery;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.GetCreatorNameQuery;

public class DetailedRecipeActivity extends CommonActivity {

    private RecyclerView recipePhotosRecyclerView;
    private DetailedRecipePhotosListAdapter detailedRecipePhotosListAdapter;
    private TextView recipeName, recipeIngredients, recipePreparation, recipeCreator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_recipe);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        recipeName = findViewById(R.id.detailed_recipe_title);
        recipeIngredients = findViewById(R.id.detailed_recipe_ingredients);
        recipePreparation = findViewById(R.id.detailed_recipe_preparation);
        recipePhotosRecyclerView = findViewById(R.id.recipe_photos_recycler_view);
        recipeCreator = findViewById(R.id.detailed_recipe_creator);

        recipePhotosRecyclerView.setNestedScrollingEnabled(false);
        recipePhotosRecyclerView.setHasFixedSize(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
        recipePhotosRecyclerView.setLayoutManager(linearLayoutManager);

        Intent intent = getIntent();

        setTitle("Przepis");
        recipeName.setText(intent.getStringExtra("recipeName"));
        recipeIngredients.setText(intent.getStringExtra("recipeIngredients"));
        recipePreparation.setText(intent.getStringExtra("recipePreparation"));

        GetCreatorNameQuery getCreatorNameQuery = new GetCreatorNameQuery();
        getCreatorNameQuery.setRecipeUid(intent.getStringExtra("recipeID"));
        userService.getCreatorName(getCreatorNameQuery, new AsyncResponse<String>() {
            @Override
            public void onSuccess(String s) {
                recipeCreator.setText(s);
            }

            @Override
            public void onException(Exception e) {

            }
        });

        GetRecipePhotosQuery getRecipePhotosQuery = new GetRecipePhotosQuery();
        getRecipePhotosQuery.setRecipeID(intent.getStringExtra("recipeID"));
        getRecipePhotosQuery.setPhotosIDs(intent.getStringArrayListExtra("recipePhotosIDs"));
        try {
            recipeService.getRecipePhotos(getRecipePhotosQuery, new AsyncResponse<ArrayList<Bitmap>>() {
                @Override
                public void onSuccess(ArrayList<Bitmap> bitmaps) {
                    detailedRecipePhotosListAdapter = new DetailedRecipePhotosListAdapter(bitmaps);
                    recipePhotosRecyclerView.setAdapter(detailedRecipePhotosListAdapter);
                    detailedRecipePhotosListAdapter.notifyDataSetChanged();
                }

                @Override
                public void onException(Exception e) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Nie ma zdjęc przepisu");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

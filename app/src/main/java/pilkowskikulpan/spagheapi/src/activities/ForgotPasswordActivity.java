package pilkowskikulpan.spagheapi.src.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.ForgotPasswordQuery;

public class ForgotPasswordActivity extends CommonActivity {
    private EditText emailEditText;
    private Button resetPasswordButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        emailEditText = findViewById(R.id.forgot_password_email);
        progressBar = findViewById(R.id.forgot_password_progress_bar);
        resetPasswordButton = findViewById(R.id.forgot_password_reset_password);

        resetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                ForgotPasswordQuery forgotPasswordQuery = new ForgotPasswordQuery();
                forgotPasswordQuery.setEmail(emailEditText.getText().toString());
                try {
                    userService.forgotPassword(forgotPasswordQuery, new AsyncResponse<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(ForgotPasswordActivity.this, "Wysłano maila", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onException(Exception e) {
                            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    });
                } catch (ForgotPasswordQuery.EmailNotValidException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Zły mail", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package pilkowskikulpan.spagheapi.src.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import com.google.firebase.auth.FirebaseUser;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.helpers.UserSharedPreferences;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.LoginUserQuery;

public class LoginActivity extends CommonActivity {
    static final int REGISTER_REQUEST = 1;
    private EditText emailEditText, passwordEditText;
    private Button registerButton, signInButton, resetPasswordButton;
    private ProgressBar progressBar;
    private UserSharedPreferences userSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userSharedPreferences = new UserSharedPreferences(getSharedPreferences("userInfo", MODE_PRIVATE));

        emailEditText = findViewById(R.id.login_email);
        passwordEditText = findViewById(R.id.login_password);
        progressBar = findViewById(R.id.login_progress_bar);
        signInButton = findViewById(R.id.login_sign_in);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginUserQuery loginUserQuery = new LoginUserQuery();
                loginUserQuery.setEmail(emailEditText.getText().toString());
                loginUserQuery.setPassword(passwordEditText.getText().toString());
                progressBar.setVisibility(View.VISIBLE);
                try {
                    userService.login(loginUserQuery, new AsyncResponse<FirebaseUser>() {
                        @Override
                        public void onSuccess(FirebaseUser firebaseUser) {
                            progressBar.setVisibility(View.GONE);
                            userSharedPreferences.saveUserDataFromFirebaseUser(firebaseUser);
                            setResult(Activity.RESULT_OK, null);
                            finish();
                        }

                        @Override
                        public void onException(Exception e) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    });
                } catch (LoginUserQuery.EmailNotValidException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Zły mail", Toast.LENGTH_SHORT).show();
                } catch (LoginUserQuery.PasswordNotValidException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Złe hasło", Toast.LENGTH_SHORT).show();
                }
            }
        });

        resetPasswordButton = findViewById(R.id.login_reset_password);
        resetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
            }
        });

        registerButton = findViewById(R.id.login_register);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getApplicationContext(), RegisterActivity.class), REGISTER_REQUEST);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REGISTER_REQUEST)
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, null);
                finish();
            }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

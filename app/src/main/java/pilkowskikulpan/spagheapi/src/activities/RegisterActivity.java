package pilkowskikulpan.spagheapi.src.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;

import com.google.firebase.auth.FirebaseUser;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.helpers.UserSharedPreferences;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.RegisterUserCommand;

public class RegisterActivity extends CommonActivity {
    private EditText emailEditText, passwordEditText, userNameEditText;
    private Button registerButton, signInButton, resetPasswordButton;
    private ProgressBar progressBar;
    private UserSharedPreferences userSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        userSharedPreferences = new UserSharedPreferences(getSharedPreferences("userInfo", MODE_PRIVATE));

        emailEditText = findViewById(R.id.register_email);
        userNameEditText = findViewById(R.id.register_username);
        passwordEditText = findViewById(R.id.register_password);
        progressBar = findViewById(R.id.register_progress_bar);
        registerButton = findViewById(R.id.register_register);
        signInButton = findViewById(R.id.register_sign_in);
        resetPasswordButton = findViewById(R.id.register_reset_password);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                RegisterUserCommand registerUserCommand = new RegisterUserCommand();
                registerUserCommand.setEmail(emailEditText.getText().toString());
                registerUserCommand.setName(userNameEditText.getText().toString());
                registerUserCommand.setPassword(passwordEditText.getText().toString());

                try {
                    userService.register(registerUserCommand,
                            new AsyncResponse<FirebaseUser>() {
                                @Override
                                public void onSuccess(FirebaseUser firebaseUser) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), "Pomyślnie zarejestrowano", Toast.LENGTH_SHORT).show();
                                    userSharedPreferences.saveUserDataFromFirebaseUser(firebaseUser);
                                    setResult(RESULT_OK, null);
                                    finish();
                                }

                                @Override
                                public void onException(Exception e) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            });
                } catch (RegisterUserCommand.EmailNotValidException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Zły mail", Toast.LENGTH_SHORT).show();
                } catch (RegisterUserCommand.PasswordNotValidException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Złe hasło", Toast.LENGTH_SHORT).show();
                }

            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        resetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}

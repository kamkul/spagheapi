package pilkowskikulpan.spagheapi.src.activities;

import androidx.appcompat.app.AppCompatActivity;

import pilkowskikulpan.spagheapi.src.favourites.DaggerFavouritesComponent;
import pilkowskikulpan.spagheapi.src.favourites.FavouritesService;
import pilkowskikulpan.spagheapi.src.recipe.DaggerRecipeComponent;
import pilkowskikulpan.spagheapi.src.recipe.RecipeService;
import pilkowskikulpan.spagheapi.src.user.DaggerUserComponent;
import pilkowskikulpan.spagheapi.src.user.UserService;

public class CommonActivity extends AppCompatActivity {

    public RecipeService recipeService;
    public UserService userService;
    public FavouritesService favouritesService;
    public CommonActivity() {
        this.recipeService = DaggerRecipeComponent.create().getService();
        this.userService = DaggerUserComponent.create().getService();
        this.favouritesService = DaggerFavouritesComponent.create().getService();
    }

}

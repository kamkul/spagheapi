package pilkowskikulpan.spagheapi.src.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.helpers.UserSharedPreferences;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.LoginUserQuery;

public class ReauthenticateActivity extends CommonActivity {

    private EditText passwordEditText;
    private Button reaunthenticateButton;
    private ProgressBar progressBar;
    private UserSharedPreferences userSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reauthentication);
        userSharedPreferences = new UserSharedPreferences(getSharedPreferences("userInfo", MODE_PRIVATE));
        userSharedPreferences.update();

        passwordEditText = findViewById(R.id.reauthenticate_password);
        progressBar = findViewById(R.id.reauthenticate_progress_bar);
        reaunthenticateButton = findViewById(R.id.reauthenticate_in);

        reaunthenticateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginUserQuery loginUserQuery = new LoginUserQuery();
                System.out.println(userSharedPreferences.getUserEmail());
                loginUserQuery.setEmail(userSharedPreferences.getUserEmail());
                loginUserQuery.setPassword(passwordEditText.getText().toString());
                progressBar.setVisibility(View.VISIBLE);
                try {
                    userService.login(loginUserQuery, new AsyncResponse<FirebaseUser>() {
                        @Override
                        public void onSuccess(FirebaseUser firebaseUser) {
                            progressBar.setVisibility(View.GONE);
                            setResult(Activity.RESULT_OK, null);
                            Toast.makeText(ReauthenticateActivity.this, "Udało się zalogować", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        @Override
                        public void onException(Exception e) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    });
                } catch (LoginUserQuery.EmailNotValidException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Zły mail", Toast.LENGTH_SHORT).show();
                } catch (LoginUserQuery.PasswordNotValidException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Złe hasło", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

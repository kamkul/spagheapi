package pilkowskikulpan.spagheapi.src.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.recipe.UpdateRecipeCommand;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;

public class EditRecipeActivity extends CommonActivity {
    private TextView recipeNameTextView, ingredientsTextView, preparationTextView;
    private Button deletePhotoButton, addPhotoButton, editRecipeButton;
    private String recipeUid;
    static final int RESULT_LOAD_IMAGE = 1;
    private ArrayList<Uri> loadedImages;
    private ArrayList<String> actualImages;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Uri selectedImage = data.getData();
            loadedImages.add(selectedImage);
            Toast.makeText(this, "Dodano zdjęcie", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipe);

        Intent intent = getIntent();
        recipeNameTextView = findViewById(R.id.edit_recipeName);
        ingredientsTextView = findViewById(R.id.edit_ingredients);
        preparationTextView = findViewById(R.id.edit_preparation);
        deletePhotoButton = findViewById(R.id.edit_delete_photo);
        addPhotoButton = findViewById(R.id.edit_add_photo);
        editRecipeButton = findViewById(R.id.edit_recipe);
        loadedImages = new ArrayList<>();
        recipeUid = intent.getStringExtra("id_przepisu");
        recipeNameTextView.setText(intent.getStringExtra("tytul"));
        ingredientsTextView.setText(intent.getStringExtra("skladniki"));
        preparationTextView.setText(intent.getStringExtra("przygotowanie"));
        actualImages = intent.getStringArrayListExtra("zdjecia");
        deletePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
        editRecipeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateRecipeCommand updateRecipeCommand = new UpdateRecipeCommand();
                updateRecipeCommand.setRecipeUid(recipeUid);
                updateRecipeCommand.setName(recipeNameTextView.getText().toString());
                updateRecipeCommand.setIngredients(ingredientsTextView.getText().toString());
                updateRecipeCommand.setPreperation(preparationTextView.getText().toString());
                updateRecipeCommand.setPhotoUris(loadedImages);
                updateRecipeCommand.setActualPhotoIds(actualImages);
                recipeService.update(updateRecipeCommand, new AsyncResponse<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(), "Zaktualizowano przepis!!!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), RecipeManagementActivity.class));
                        finish();
                    }

                    @Override
                    public void onException(Exception e) {
                        Toast.makeText(getApplicationContext(), "UPS", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

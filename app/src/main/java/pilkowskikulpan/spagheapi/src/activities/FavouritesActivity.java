package pilkowskikulpan.spagheapi.src.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.adapters.FavouriteListAdapter;
import pilkowskikulpan.spagheapi.src.favourites.DeleteFromFavouritesCommand;
import pilkowskikulpan.spagheapi.src.recipe.Recipe;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;

public class FavouritesActivity extends CommonActivity {

    public FavouriteListAdapter favouriteListAdapter;
    public RecyclerView favouriteListRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        favouriteListRecyclerView = findViewById(R.id.favourite_recipes);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        favouriteListRecyclerView.setNestedScrollingEnabled(false);
        favouriteListRecyclerView.setHasFixedSize(false);
        favouriteListRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        this.favouritesService.showYourFavourites(new AsyncResponse<ArrayList<Recipe>>() {
            @Override
            public void onSuccess(final ArrayList<Recipe> recipes) {
                favouriteListAdapter = new FavouriteListAdapter(recipes);
                favouriteListRecyclerView.setAdapter(favouriteListAdapter);
                favouriteListAdapter.notifyDataSetChanged();

                favouriteListAdapter.setOnItemClickListener(new FavouriteListAdapter.OnItemClickListener() {

                    @Override
                    public void onDeleted(final int position) {
                        DeleteFromFavouritesCommand deleteFromFavouritesCommand = new DeleteFromFavouritesCommand();
                        deleteFromFavouritesCommand.setRecipeUid(recipes.get(position).getuId());
                        favouritesService.deleteFromFavourites(deleteFromFavouritesCommand, new AsyncResponse<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                recipes.remove(position);
                                favouriteListAdapter.notifyDataSetChanged();
                                Toast.makeText(getApplicationContext(), "Usunięto z ulubionych", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onException(Exception exception) {

                            }
                        });
                    }
                });
            }

            @Override
            public void onException(Exception e) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

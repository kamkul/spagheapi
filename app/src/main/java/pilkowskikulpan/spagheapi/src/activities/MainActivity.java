package pilkowskikulpan.spagheapi.src.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.FirebaseApp;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.adapters.RecipeListAdapter;

import pilkowskikulpan.spagheapi.src.favourites.AddToFavourtiesCommand;
import pilkowskikulpan.spagheapi.src.favourites.DeleteFromFavouritesCommand;
import pilkowskikulpan.spagheapi.src.helpers.UserSharedPreferences;
import pilkowskikulpan.spagheapi.src.recipe.Recipe;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.GetUserPhotoFromStorageQuery;
import pilkowskikulpan.spagheapi.src.user.UserNotLoggedInException;

public class MainActivity extends CommonActivity implements NavigationView.OnNavigationItemSelectedListener {

    static final int LOGIN_REQUEST_FOR_PROFILE_SETTINGS = 1;
    static final int LOGIN_REQUEST_FOR_ADD_RECIPE = 2;
    private FloatingActionButton addRecipeFloatingButton;
    private TextView userNameTextView;
    private TextView userEmailTextView;
    private ImageView userImageView;
    private RecyclerView recipeListRecyclerView;
    private RecipeListAdapter recipeListAdapter;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private UserSharedPreferences userSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseApp.initializeApp(this);
        userSharedPreferences = new UserSharedPreferences(getSharedPreferences("userInfo", MODE_PRIVATE));

        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        recipeListRecyclerView = findViewById(R.id.recipe_list);
        addRecipeFloatingButton = findViewById(R.id.addRecipeInitialize);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawerLayout.addDrawerListener(toggle);
        DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                userNameTextView = findViewById(R.id.userName);
                userEmailTextView = findViewById(R.id.userEmail);
                userImageView = findViewById(R.id.imageView);
                userSharedPreferences.update();
//                String uri = userSharedPreferences.getUserPhotoURL();
                GetUserPhotoFromStorageQuery getUserPhotoFromStorageQuery = new GetUserPhotoFromStorageQuery();
                try {
                    getUserPhotoFromStorageQuery.setUser(userService.getCurrentUser());
                    userNameTextView.setText(userSharedPreferences.getUserName());
                    userEmailTextView.setText(userSharedPreferences.getUserEmail());
                    userService.getUserPhotoFromStorage(getUserPhotoFromStorageQuery, new AsyncResponse<Bitmap>() {
                        @Override
                        public void onSuccess(Bitmap bitmap) {
                            userImageView.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onException(Exception e) {
                            Toast.makeText(MainActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (UserNotLoggedInException e) {
                    userEmailTextView.setText(new StringBuilder("Niezalogowano"));
                    e.printStackTrace();
                }
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        };

        drawerLayout.addDrawerListener(drawerListener);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        recipeListRecyclerView.setNestedScrollingEnabled(false);
        recipeListRecyclerView.setHasFixedSize(false);
        recipeListRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));

        try {
            this.recipeService.getAll(new AsyncResponse<ArrayList<Recipe>>() {
                @Override
                public void onSuccess(final ArrayList<Recipe> recipes) {
                    recipeListAdapter = new RecipeListAdapter(recipes);
                    recipeListRecyclerView.setAdapter(recipeListAdapter);
                    recipeListAdapter.notifyDataSetChanged();

                    recipeListAdapter.setOnItemClickListener(new RecipeListAdapter.OnItemClickListener() {
                        @Override
                        public void onAdded(int position) {
                            AddToFavourtiesCommand addToFavourtiesCommand = new AddToFavourtiesCommand();
                            addToFavourtiesCommand.setRecipeUid(recipes.get(position).getuId());
                            favouritesService.addToFavourites(addToFavourtiesCommand, new AsyncResponse<Void>() {

                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getApplicationContext(), "Dodano do ulubionych", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onException(Exception exception) {

                                }
                            });
                        }

                        @Override
                        public void onDeleted(int position) {
                            DeleteFromFavouritesCommand deleteFromFavouritesCommand = new DeleteFromFavouritesCommand();
                            deleteFromFavouritesCommand.setRecipeUid(recipes.get(position).getuId());
                            favouritesService.deleteFromFavourites(deleteFromFavouritesCommand, new AsyncResponse<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getApplicationContext(), "Usunięto z ulubionych", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onException(Exception exception) {

                                }
                            });
                        }
                    });

                }

                @Override
                public void onException(Exception exception) {
                    exception.printStackTrace();
                }
            });

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Coś poszło nie tak", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        addRecipeFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    userService.getCurrentUser();
                    startActivity(new Intent(getApplicationContext(), AddRecipeActivity.class));
                } catch (UserNotLoggedInException e) {
                    startActivityForResult(new Intent(MainActivity.this, LoginActivity.class),
                            LOGIN_REQUEST_FOR_ADD_RECIPE);
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == LOGIN_REQUEST_FOR_PROFILE_SETTINGS) {
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(this, ProfileSettingsActivity.class));
            } else
                Toast.makeText(this, "Musisz się zalogować, aby kontynuować", Toast.LENGTH_SHORT).show();
        }
        if (requestCode == LOGIN_REQUEST_FOR_ADD_RECIPE) {
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(this, AddRecipeActivity.class));
            } else
                Toast.makeText(this, "Musisz się zalogować, aby kontynuować", Toast.LENGTH_SHORT).show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        userNameTextView = findViewById(R.id.userName);
        switch (item.getItemId()) {
            case R.id.nav_recipe_management:
                startActivity(new Intent(this, RecipeManagementActivity.class));
                break;
            case R.id.nav_favourites:
                startActivity(new Intent(this, FavouritesActivity.class));
                break;
            case R.id.nav_profile:
                try {
                    userService.getCurrentUser();
                    startActivity(new Intent(this, ProfileSettingsActivity.class));
                } catch (UserNotLoggedInException e) {
                    startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST_FOR_PROFILE_SETTINGS);
                    e.printStackTrace();
                }
                break;
            case R.id.nav_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.nav_sign_out:
                try {
                    userService.getCurrentUser();
                    userService.logout(new AsyncResponse<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            userSharedPreferences.deleteUserData();
                            userNameTextView.setText(new StringBuilder("Niezalogowano"));
                            Toast.makeText(getApplicationContext(), "Wylogowano", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onException(Exception e) {

                        }
                    });
                } catch (UserNotLoggedInException e) {
                    e.printStackTrace();
                }
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

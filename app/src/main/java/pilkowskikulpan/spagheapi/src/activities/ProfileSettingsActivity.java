package pilkowskikulpan.spagheapi.src.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.helpers.UserSharedPreferences;
import pilkowskikulpan.spagheapi.src.recipe.DaggerRecipeComponent;
import pilkowskikulpan.spagheapi.src.recipe.RecipeService;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;
import pilkowskikulpan.spagheapi.src.user.DaggerUserComponent;
import pilkowskikulpan.spagheapi.src.user.SetPasswordQuery;
import pilkowskikulpan.spagheapi.src.user.SetUserEmailCommand;
import pilkowskikulpan.spagheapi.src.user.SetUserNameCommand;
import pilkowskikulpan.spagheapi.src.user.SetUserPhotoCommand;
import pilkowskikulpan.spagheapi.src.user.UserNotLoggedInException;
import pilkowskikulpan.spagheapi.src.user.UserService;

public class ProfileSettingsActivity extends CommonActivity {

    static final int RESULT_LOAD_IMAGE = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            final ProgressDialog progressDialog = new ProgressDialog(ProfileSettingsActivity.this);
            progressDialog.show();
            Uri selectedImage = data.getData();
            SetUserPhotoCommand setUserPhotoCommand = new SetUserPhotoCommand();
            setUserPhotoCommand.setPath(selectedImage);
            try {
                setUserPhotoCommand.setUser(userService.getCurrentUser());
                userService.setUserPhoto(setUserPhotoCommand, new AsyncResponse<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Toast.makeText(ProfileSettingsActivity.this, "HURRA", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onException(Exception e) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        if (e instanceof FirebaseAuthRecentLoginRequiredException)
                            startActivity(new Intent(ProfileSettingsActivity.this, ReauthenticateActivity.class));
                        Toast.makeText(ProfileSettingsActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (UserNotLoggedInException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {

        UserSharedPreferences userSharedPreferences;
        private RecipeService recipeService;
        private UserService userService;

        public SettingsFragment() {
            this.recipeService = DaggerRecipeComponent.create().getService();
            this.userService = DaggerUserComponent.create().getService();
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.profile_settings_preferences, rootKey);

            userSharedPreferences = new UserSharedPreferences(getContext().getSharedPreferences("userInfo", MODE_PRIVATE));
            userSharedPreferences.update();

            Preference change_avatar_preference = findPreference("userPhoto");
            final EditTextPreference userName = findPreference("userName");
            final EditTextPreference userPassword = findPreference("userPassword");
            final EditTextPreference userEmail = findPreference("userEmail");

            userPassword.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    SetPasswordQuery setPasswordQuery = new SetPasswordQuery();
                    setPasswordQuery.setPassword(newValue.toString());
                    try {
                        setPasswordQuery.setUser(userService.getCurrentUser());
                        final ProgressDialog progressDialog = new ProgressDialog(getContext());
                        progressDialog.show();
                        userService.setUserPassword(setPasswordQuery, new AsyncResponse<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                Toast.makeText(getContext(), "Dobrze", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onException(Exception e) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                if (e instanceof FirebaseAuthRecentLoginRequiredException)
                                    startActivity(new Intent(getContext(), ReauthenticateActivity.class));
                                Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (UserNotLoggedInException e) {
                        e.printStackTrace();
                    }
                    return false;
                }
            });

            if (userName != null) {
                userName.setText(userSharedPreferences.getUserName());
            }
            if (userEmail != null) {
                userEmail.setText(userSharedPreferences.getUserEmail());
            }

            if (userName != null) {
                userName.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(final Preference preference, final Object newValue) {
                        SetUserNameCommand setUserNameCommand = new SetUserNameCommand();
                        setUserNameCommand.setDisplayName(newValue.toString());
                        try {
                            setUserNameCommand.setUser(userService.getCurrentUser());
                            final ProgressDialog progressDialog = new ProgressDialog(getContext());
                            progressDialog.show();
                            userService.setUserName(setUserNameCommand, new AsyncResponse<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    userSharedPreferences.setUserName(newValue.toString());
                                    userSharedPreferences.saveUserData();
                                    Toast.makeText(getContext(), "Dobrze", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onException(Exception e) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    if (e instanceof FirebaseAuthRecentLoginRequiredException)
                                        startActivity(new Intent(getContext(), ReauthenticateActivity.class));
                                    Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (UserNotLoggedInException e) {
                            e.printStackTrace();
                        }

                        return true;
                    }
                });
            }

            if (userEmail != null) {
                userEmail.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(final Preference preference, final Object newValue) {
                        SetUserEmailCommand setUserEmailCommand = new SetUserEmailCommand();
                        setUserEmailCommand.setEmail(newValue.toString());
                        try {
                            setUserEmailCommand.setUser(userService.getCurrentUser());
                            final ProgressDialog progressDialog = new ProgressDialog(getContext());
                            progressDialog.show();
                            userService.setUserEmail(setUserEmailCommand, new AsyncResponse<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    userSharedPreferences.setUserEmail(newValue.toString());
                                    userSharedPreferences.saveUserData();
                                    Toast.makeText(getContext(), "Dobrze", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onException(Exception e) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    if (e instanceof FirebaseAuthRecentLoginRequiredException)
                                        startActivity(new Intent(getContext(), ReauthenticateActivity.class));
                                    Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (UserNotLoggedInException e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                });
            }

            if (change_avatar_preference != null) {
                change_avatar_preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, RESULT_LOAD_IMAGE);
                        return true;
                    }
                });
            }

        }
    }
}
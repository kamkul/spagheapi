package pilkowskikulpan.spagheapi.src.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pilkowskikulpan.spagheapi.R;
import pilkowskikulpan.spagheapi.src.adapters.YourRecipeListAdapter;
import pilkowskikulpan.spagheapi.src.recipe.DeleteRecipeQuery;
import pilkowskikulpan.spagheapi.src.recipe.Recipe;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;

public class RecipeManagementActivity extends CommonActivity {
    private RecyclerView recipeListRecyclerView;
    private YourRecipeListAdapter yourRecipeListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_management);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        recipeListRecyclerView = findViewById(R.id.user_recipes);
        recipeListRecyclerView.setNestedScrollingEnabled(false);
        recipeListRecyclerView.setHasFixedSize(false);
        recipeListRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        try {
            this.recipeService.getAllYourRecipes(new AsyncResponse<ArrayList<Recipe>>() {
                @Override
                public void onSuccess(final ArrayList<Recipe> recipes) {
                    yourRecipeListAdapter = new YourRecipeListAdapter(recipes);
                    recipeListRecyclerView.setAdapter(yourRecipeListAdapter);
                    yourRecipeListAdapter.notifyDataSetChanged();

                    yourRecipeListAdapter.setOnItemClickListener(new YourRecipeListAdapter.OnItemClickListener() {
                        @Override
                        public void onEdited(int position) {
                            Recipe recipe = recipes.get(position);
                            Intent intent = new Intent(getApplicationContext(), EditRecipeActivity.class);
                            intent.putExtra("id_przepisu", recipe.getuId());
                            intent.putExtra("tytul", recipe.getName());
                            intent.putExtra("skladniki", recipe.getIngredients());
                            intent.putExtra("przygotowanie", recipe.getPreperation());
                            intent.putStringArrayListExtra("zdjecia", recipe.getPhotoIDs());
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onDeleted(final int position) {
                            final AlertDialog.Builder alert=new AlertDialog.Builder(RecipeManagementActivity.this);
                            alert.setMessage("Czy na pewno chcesz usunąć przepis?").setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Recipe recipe= recipes.get(position);
                                            DeleteRecipeQuery deleteRecipeQuery = new DeleteRecipeQuery();
                                            deleteRecipeQuery.setRecipeUid(recipe.getuId());
                                            recipeService.delete(deleteRecipeQuery, new AsyncResponse<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    recipes.remove(position);
                                                    yourRecipeListAdapter.notifyItemRemoved(position);
                                                    Toast.makeText(getApplicationContext(), "Usunięto przepis", Toast.LENGTH_SHORT).show();

                                                }

                                                @Override
                                                public void onException(Exception exception) {

                                                }
                                            });
                                        }
                                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            }).show();
                        }
                    });
                }

                @Override
                public void onException(Exception e) {

                }

            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Ups", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

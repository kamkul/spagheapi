package pilkowskikulpan.spagheapi.src.recipe;

import android.net.Uri;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class AddRecipeCommand {

    @Nullable
    private String name;
    @Nullable
    private String ingredients;
    @Nullable
    private String preperation;
    @Nullable
    private String creator;

    @Nullable
    private
    ArrayList<Uri> photoUris;

    public void setPhotoUris(@Nullable ArrayList<Uri> photoUris) {
        this.photoUris = photoUris;
    }
    @Nullable
    private
    Uri path;

    @Nullable
    public Uri getPath() {
        return path;
    }

    public void setPath(@Nullable Uri path) {
        this.path = path;
    }

    @Nullable
    ArrayList<Uri> getPhotoUris() {
        return photoUris;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    String getIngredients() {
        return ingredients;
    }

    public void setIngredients(@Nullable String ingredients) {
        this.ingredients = ingredients;
    }

    @Nullable
    String getPreperation() {
        return preperation;
    }

    public void setPreperation(@Nullable String preperation) {
        this.preperation = preperation;
    }

    @Nullable
    String getCreator() {
        return creator;
    }

    public void setCreator(@Nullable String creator) {
        this.creator = creator;
    }

    public boolean isValid() throws Exception {
        return false;
    }
}

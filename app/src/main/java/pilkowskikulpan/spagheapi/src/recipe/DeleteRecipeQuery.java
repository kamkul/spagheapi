package pilkowskikulpan.spagheapi.src.recipe;

import androidx.annotation.Nullable;

public class DeleteRecipeQuery {
    @Nullable
    private String recipeUid;

    @Nullable
    public String getRecipeUid() {
        return recipeUid;
    }

    public void setRecipeUid(@Nullable String recipeUid) {
        this.recipeUid = recipeUid;
    }

    public boolean isValid() throws Exception{
        return false;
    }
}

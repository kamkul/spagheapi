package pilkowskikulpan.spagheapi.src.recipe;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class GetRecipePhotosQuery {
    @Nullable
    private String recipeID;

    @Nullable
    private ArrayList<String> photosIDs;

    @Nullable
    String getRecipeID() {
        return recipeID;
    }

    public void setRecipeID(@Nullable String recipeID) {
        this.recipeID = recipeID;
    }

    @Nullable
    ArrayList<String> getPhotosIDs() {
        return photosIDs;
    }

    public void setPhotosIDs(@Nullable ArrayList<String> photosIDs) {
        this.photosIDs = photosIDs;
    }

    public void isValid() throws Exception {
        if(photosIDs == null)
            throw new Exception();
    }
}

package pilkowskikulpan.spagheapi.src.recipe;

import android.net.Uri;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class UpdateRecipeCommand {

    @Nullable
    private
    String recipeUid;
    @Nullable
    private
    String name;
    @Nullable
    private
    String ingredients;
    @Nullable
    private
    String preperation;
    @Nullable
    private ArrayList<Uri> photoUris;
    @Nullable
    private ArrayList<String> actualPhotoIds;
    @Nullable
    public String getRecipeUid() {
        return recipeUid;
    }

    public void setRecipeUid(@Nullable String recipeUid) {
        this.recipeUid = recipeUid;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    String getIngredients() {
        return ingredients;
    }

    public void setIngredients(@Nullable String ingredients) {
        this.ingredients = ingredients;
    }

    @Nullable
    String getPreperation() {
        return preperation;
    }

    public void setPreperation(@Nullable String preperation) {
        this.preperation = preperation;
    }


    @Nullable
    ArrayList<Uri> getPhotoUris() {
        return photoUris;
    }

    public void setPhotoUris(@Nullable ArrayList<Uri> photoUris) {
        this.photoUris = photoUris;
    }

    @Nullable
    ArrayList<String> getActualPhotoIds() {
        return actualPhotoIds;
    }

    public void setActualPhotoIds(@Nullable ArrayList<String> actualPhotoIds) {
        this.actualPhotoIds = actualPhotoIds;
    }
}

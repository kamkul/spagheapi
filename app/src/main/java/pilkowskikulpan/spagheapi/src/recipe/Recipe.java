package pilkowskikulpan.spagheapi.src.recipe;


import androidx.annotation.Nullable;

import java.util.ArrayList;

public class Recipe {
    @Nullable
    private String uId;
    @Nullable
    private String name;
    @Nullable
    private String ingredients;
    @Nullable
    private String preperation;
    @Nullable
    private ArrayList<String> photoIDs;
    @Nullable
    private String creator;

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(@Nullable String ingredients) {
        this.ingredients = ingredients;
    }

    @Nullable
    public String getPreperation() {
        return preperation;
    }

    public void setPreperation(@Nullable String preperation) {
        this.preperation = preperation;
    }

    @Nullable
    public ArrayList<String> getPhotoIDs() {
        return photoIDs;
    }

    public void setPhotoIDs(@Nullable ArrayList<String> photoIDs) {
        this.photoIDs = photoIDs;
    }

    @Nullable
    public String getuId() {
        return uId;
    }

    public void setuId(@Nullable String uId) {
        this.uId = uId;
    }

    @Override
    public String toString() {
        return uId + " " + name + " " + ingredients + " " + preperation;
    }

    @Nullable
    public String getCreator() {
        return creator;
    }

    public void setCreator(@Nullable String creator) {
        this.creator = creator;
    }
}

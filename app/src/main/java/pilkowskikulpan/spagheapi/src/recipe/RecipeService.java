package pilkowskikulpan.spagheapi.src.recipe;

import android.graphics.Bitmap;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.Module;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;

@Module
public class RecipeService {

    private
    RecipeRepository recipeRepository;

    @Inject
    RecipeService() {
        RecipeComponent recipeComponent = DaggerRecipeComponent.create();
        this.recipeRepository = recipeComponent.getRepository();
    }

    public void addRecipe(AddRecipeCommand addRecipeCommand, AsyncResponse<Void> asyncResponse) throws Exception {
        addRecipeCommand.isValid();
        recipeRepository.addRecipe(addRecipeCommand, asyncResponse);
    }

    public void getAll(AsyncResponse<ArrayList<Recipe>> asyncResponse) {
        recipeRepository.getAll(asyncResponse);
    }

    public void getAllYourRecipes(AsyncResponse<ArrayList<Recipe>> asyncResponse) {
        recipeRepository.getAllYourRecipes(asyncResponse);
    }

    public void delete(DeleteRecipeQuery deleteRecipeQuery, AsyncResponse<Void> asyncResponse) {
        recipeRepository.delete(deleteRecipeQuery, asyncResponse);
    }
    public void update(UpdateRecipeCommand updateRecipeCommand, AsyncResponse<Void> asyncResponse) {
        this.recipeRepository.update(updateRecipeCommand, asyncResponse);
    }

    public void getRecipePhotos(GetRecipePhotosQuery getRecipePhotosQuery, AsyncResponse<ArrayList<Bitmap>> asyncResponse) throws Exception {
        getRecipePhotosQuery.isValid();
        recipeRepository.getRecipePhotos(getRecipePhotosQuery, asyncResponse);
    }
}

package pilkowskikulpan.spagheapi.src.recipe;

import androidx.annotation.Nullable;

public class GetAllPhotosForRecipe {

    @Nullable
    private Recipe recipe;

    @Nullable
    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(@Nullable Recipe recipe) {
        this.recipe = recipe;
    }
}

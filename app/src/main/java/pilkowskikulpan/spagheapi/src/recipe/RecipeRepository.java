package pilkowskikulpan.spagheapi.src.recipe;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import dagger.Module;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;

@Module
class RecipeRepository {

    private static final String TAG = "RecipeRepository";
    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;
    private ArrayList<Recipe> output = new ArrayList<>();
    private FirebaseAuth firebaseAuth;
    private DatabaseReference recipesDatabase;

    @Inject
    RecipeRepository() {
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.recipesDatabase = FirebaseDatabase.getInstance().getReference().child("recipes");
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.firebaseStorage = FirebaseStorage.getInstance();
    }

    void getAll(final AsyncResponse<ArrayList<Recipe>> asyncResponse) {
        Query query = recipesDatabase.orderByChild("recipeUid");

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        Recipe mRecipe = new Recipe();
                        mRecipe.setuId(childSnapshot.getKey());
                        mRecipe.setName(childSnapshot.child("name").getValue().toString());
                        mRecipe.setIngredients(childSnapshot.child("ingredients").getValue().toString());
                        mRecipe.setPreperation(childSnapshot.child("preperation").getValue().toString());
                        mRecipe.setPhotoIDs((ArrayList<String>) childSnapshot.child("photoIDs").getValue());
                        output.add(mRecipe);
                    }
                }
                asyncResponse.onSuccess(output);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                asyncResponse.onException(databaseError.toException());
            }
        });
    }

    void addRecipe(final AddRecipeCommand addRecipeCommand, final AsyncResponse<Void> asyncResponse) {
//        addRecipeCommand.setCreator(firebaseAuth.getCurrentUser().getUid());
//        DatabaseReference recipesDatabase=firebaseDatabase.getReference().child("recipes");
        final String recipeUid = recipesDatabase.push().getKey();
        addRecipeCommand.setCreator(firebaseAuth.getCurrentUser().getUid());

        final ArrayList<String> photoIDs = new ArrayList<>();
        ArrayList<Task<UploadTask.TaskSnapshot>> uploadPhotosTask = new ArrayList<>();

        for (Uri uri : addRecipeCommand.getPhotoUris()) {
            final String photoID = firebaseDatabase.getReference().push().getKey();
            StorageReference photosStorage = firebaseStorage.getReference()
                    .child("recipes/" + recipeUid + "/photos/" + photoID);
            uploadPhotosTask.add(photosStorage.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    photoIDs.add(photoID);
                    asyncResponse.onSuccess(null);
                }
            }));
        }

        if (uploadPhotosTask.size() > 0)
            Tasks.whenAll(uploadPhotosTask).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Recipe recipe = new Recipe();
                    recipe.setIngredients(addRecipeCommand.getIngredients());
                    recipe.setCreator(addRecipeCommand.getCreator());
                    recipe.setName(addRecipeCommand.getName());
                    recipe.setPreperation(addRecipeCommand.getPreperation());
                    recipe.setPhotoIDs(photoIDs);

                    recipesDatabase.child(recipeUid).setValue(recipe).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            asyncResponse.onException(e);
                        }
                    });
                }
            });
        else {
            Recipe recipe = new Recipe();
            recipe.setIngredients(addRecipeCommand.getIngredients());
            recipe.setCreator(addRecipeCommand.getCreator());
            recipe.setName(addRecipeCommand.getName());
            recipe.setPreperation(addRecipeCommand.getPreperation());
            recipesDatabase.child(recipeUid).setValue(recipe).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    asyncResponse.onSuccess(aVoid);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    asyncResponse.onException(e);
                }
            });
        }
    }

    void getAllYourRecipes(final AsyncResponse<ArrayList<Recipe>> asyncResponse) {
        final String currentUser = firebaseAuth.getCurrentUser().getUid();
        Query query = recipesDatabase.orderByChild("recipeUid");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        if (currentUser.equals(childSnapshot.child("creator").getValue().toString())) {
                            Recipe mRecipe = new Recipe();
                            mRecipe.setuId(childSnapshot.getKey());
                            mRecipe.setName(childSnapshot.child("name").getValue().toString());
                            mRecipe.setIngredients(childSnapshot.child("ingredients").getValue().toString());
                            mRecipe.setPreperation(childSnapshot.child("preperation").getValue().toString());
                            mRecipe.setPhotoIDs((ArrayList<String>) childSnapshot.child("photoIDs").getValue());
                            output.add(mRecipe);
                        }
                    }
                    asyncResponse.onSuccess(output);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                asyncResponse.onException(databaseError.toException());
            }
        });
    }

    void getRecipePhotos(GetRecipePhotosQuery getRecipePhotosQuery, final AsyncResponse<ArrayList<Bitmap>> asyncResponse) {

        ArrayList<Task<FileDownloadTask.TaskSnapshot>> uploadPhotosTask = new ArrayList<>();
        final ArrayList<Bitmap> output = new ArrayList<>();

        for (String photoID : getRecipePhotosQuery.getPhotosIDs()) {
            StorageReference photosStorage = firebaseStorage.getReference()
                    .child("recipes/" + getRecipePhotosQuery.getRecipeID() + "/photos/" + photoID);

            try {
                final File localFile = File.createTempFile(photoID, "jpg");
                uploadPhotosTask.add(
                        photosStorage.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                                output.add(bitmap);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                asyncResponse.onException(exception);
                            }
                        }));
            } catch (IOException e) {
                e.printStackTrace();
            }

            Tasks.whenAll(uploadPhotosTask).addOnSuccessListener(
                    new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            asyncResponse.onSuccess(output);
                        }
                    }
            );
        }
    }

    void delete(final DeleteRecipeQuery deleteRecipeQuery, final AsyncResponse<Void> asyncResponse) {
        recipesDatabase.child(deleteRecipeQuery.getRecipeUid()).getRef().removeValue().
                addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        asyncResponse.onSuccess(aVoid);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                asyncResponse.onException(e);
            }
        });
    }

    void update(final UpdateRecipeCommand updateRecipeCommand, final AsyncResponse<Void> asyncResponse) {
        final ArrayList<String> photoIDs;
        if (updateRecipeCommand.getActualPhotoIds() == null) {
            photoIDs = new ArrayList<>();
        } else {
            photoIDs = updateRecipeCommand.getActualPhotoIds();
        }
        ArrayList<Task<UploadTask.TaskSnapshot>> uploadPhotosTask = new ArrayList<>();
        for (Uri uri : updateRecipeCommand.getPhotoUris()) {
            final String photoID = firebaseDatabase.getReference().push().getKey();
            StorageReference photosStorage = firebaseStorage.getReference()
                    .child("recipes/" + updateRecipeCommand.getRecipeUid() + "/photos/" + photoID);
            uploadPhotosTask.add(photosStorage.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    photoIDs.add(photoID);
                    asyncResponse.onSuccess(null);
                }
            }));
        }
        if (uploadPhotosTask.size() > 0) {
            Tasks.whenAll(uploadPhotosTask).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    recipesDatabase.child(updateRecipeCommand.getRecipeUid()).child("name").getRef().setValue(updateRecipeCommand.getName());
                    recipesDatabase.child(updateRecipeCommand.getRecipeUid()).child("ingredients").getRef().setValue(updateRecipeCommand.getIngredients());
                    recipesDatabase.child(updateRecipeCommand.getRecipeUid()).child("preperation").getRef().setValue(updateRecipeCommand.getPreperation());
                    recipesDatabase.child(updateRecipeCommand.getRecipeUid()).child("photoIDs").getRef().setValue(photoIDs).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            asyncResponse.onException(e);
                        }
                    });
                }
            });
        } else {
            recipesDatabase.child(updateRecipeCommand.getRecipeUid()).child("name").getRef().setValue(updateRecipeCommand.getName());
            recipesDatabase.child(updateRecipeCommand.getRecipeUid()).child("ingredients").getRef().setValue(updateRecipeCommand.getIngredients());
            recipesDatabase.child(updateRecipeCommand.getRecipeUid()).child("preperation").getRef().setValue(updateRecipeCommand.getPreperation()).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    asyncResponse.onSuccess(aVoid);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    asyncResponse.onException(e);
                }
            });
        }
    }

}

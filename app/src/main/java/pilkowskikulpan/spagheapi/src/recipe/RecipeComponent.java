package pilkowskikulpan.spagheapi.src.recipe;

import dagger.Component;

@Component
public interface RecipeComponent {

    RecipeRepository getRepository();

    RecipeService getService();

}

package pilkowskikulpan.spagheapi.src.helpers;

import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseUser;

public class UserSharedPreferences {

    private SharedPreferences sharedPreferences;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhotoURL() {
        return userPhotoURL;
    }

    public void setUserPhotoURL(String userPhotoURL) {
        this.userPhotoURL = userPhotoURL;
    }

    public String getUserDeviceToken() {
        return userDeviceToken;
    }

    private String userName;
    private String userID;
    private String userEmail;
    private String userPhotoURL;
    private String userDeviceToken;

    public UserSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void setUserDeviceToken(String userDeviceToken) {
        this.userDeviceToken = userDeviceToken;
    }

    public void saveUserDataFromFirebaseUser(FirebaseUser firebaseUser) {

        if (firebaseUser != null) {
            String name = firebaseUser.getDisplayName();
            String email = firebaseUser.getEmail();
            String userId = firebaseUser.getUid();
            String uriString;
            String token = firebaseUser.getIdToken(true).toString();

            System.out.println(firebaseUser.describeContents());

            if (firebaseUser.getPhotoUrl() == null) {
                uriString = "";
            } else {
                uriString = firebaseUser.getPhotoUrl().toString();
            }

            this.setUserName(name);
            this.setUserID(userId);
            this.setUserEmail(email);
            this.setUserPhotoURL(uriString);
            this.setUserDeviceToken(token);

            sharedPreferences.edit()
                    .putString("userName", name)
                    .putString("userID", userId)
                    .putString("userEmail", email)
                    .putString("userPhotoURL", userPhotoURL)
                    .putString("userDeviceToken", userDeviceToken).apply();
        }
    }

    public void saveUserData() {
        sharedPreferences.edit()
                .putString("userName", userName)
                .putString("userID", userID)
                .putString("userEmail", userEmail)
                .putString("userPhotoURL", userPhotoURL)
                .putString("userDeviceToken", userDeviceToken).apply();
    }

    public void update(){
        userName = sharedPreferences.getString("userName", "");
        userID = sharedPreferences.getString("userID", "");
        userEmail = sharedPreferences.getString("userEmail", "");
        userPhotoURL = sharedPreferences.getString("userPhotoURL", "");
        userDeviceToken = sharedPreferences.getString("userDeviceToken", "");
    }

    public void deleteUserData(){
        this.sharedPreferences.edit().clear().apply();
    }
}

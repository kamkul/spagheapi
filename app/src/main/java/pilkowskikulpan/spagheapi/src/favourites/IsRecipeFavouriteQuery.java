package pilkowskikulpan.spagheapi.src.favourites;

import androidx.annotation.Nullable;

public class IsRecipeFavouriteQuery {
    @Nullable
    private String recipeUid;

    @Nullable
    public String getRecipeUid() {
        return recipeUid;
    }

    public void setRecipeUid(@Nullable String recipeUid) {
        this.recipeUid = recipeUid;
    }
}

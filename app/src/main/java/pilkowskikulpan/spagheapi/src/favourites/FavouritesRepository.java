package pilkowskikulpan.spagheapi.src.favourites;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.Module;
import pilkowskikulpan.spagheapi.src.recipe.Recipe;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;

@Module
public class FavouritesRepository {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private DatabaseReference userDatabase;
    @Inject
    FavouritesRepository() {
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.databaseReference = FirebaseDatabase.getInstance().getReference();
        this.userDatabase = FirebaseDatabase.getInstance().getReference().child("users");
    }

    void addToFavourites(final AddToFavourtiesCommand addToFavourtiesCommand,
                         final AsyncResponse<Void> asyncResponse) {
        final ArrayList<String> favourites = new ArrayList<>();
        databaseReference.child("users").child(firebaseAuth.getCurrentUser().getUid()).child("favourites").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        favourites.add(childSnapshot.getValue().toString());
                    }
                }
                favourites.add(addToFavourtiesCommand.getRecipeUid());
                databaseReference.child("users").child(firebaseAuth.getCurrentUser().getUid()).child("favourites").setValue(favourites).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        asyncResponse.onSuccess(aVoid);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        asyncResponse.onException(e);
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                asyncResponse.onException(databaseError.toException());
            }
        });
    }

    void deleteFromFavourites(final DeleteFromFavouritesCommand deleteFromFavourites,
                              final AsyncResponse<Void> asyncResponse) {
        final ArrayList<String> favourites = new ArrayList<>();
        databaseReference.child("users").child(firebaseAuth.getCurrentUser().getUid()).
                child("favourites").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        favourites.add(childSnapshot.getValue().toString());
                    }
                }
                favourites.remove(deleteFromFavourites.getRecipeUid());
                databaseReference.child("users").child(firebaseAuth.getCurrentUser().getUid()).child("favourites")
                        .setValue(favourites).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        asyncResponse.onSuccess(aVoid);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        asyncResponse.onException(e);
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                asyncResponse.onException(databaseError.toException());
            }
        });
    }

    void showYourFavourites(final AsyncResponse<ArrayList<Recipe>> asyncResponse) {
        final ArrayList<String> favourites = new ArrayList<>();
        final ArrayList<Recipe> recipes = new ArrayList<>();
        databaseReference.child("users").child(firebaseAuth.getCurrentUser().getUid())
                .child("favourites").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        favourites.add(childSnapshot.getValue().toString());
                    }
                }
                Log.d("fav", favourites.toString());
                databaseReference.child("recipes").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {

                                for (String recipeUid : favourites) {
                                    if (recipeUid.equals(childSnapshot.getKey())) {

                                        Recipe recipe = new Recipe();
                                        recipe.setuId(childSnapshot.getKey());
                                        recipe.setName(childSnapshot.child("name").getValue().toString());
                                        recipe.setIngredients(childSnapshot.child("ingredients").getValue().toString());
                                        recipe.setPreperation(childSnapshot.child("preperation").getValue().toString());
                                        recipe.setPhotoIDs((ArrayList<String>) childSnapshot.child("photoIDs").getValue());
                                        recipes.add(recipe);

                                    }
                                }
                            }
                        }

                        asyncResponse.onSuccess(recipes);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        asyncResponse.onException(databaseError.toException());
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                asyncResponse.onException(databaseError.toException());
            }
        });
    }

    void isFavourite(final IsRecipeFavouriteQuery isRecipeFavouriteQuery, final AsyncResponse<Boolean> asyncResponse) {
        userDatabase.child(firebaseAuth.getCurrentUser().getUid()).child("favourites")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot childsnapshot : dataSnapshot.getChildren()) {
                                if (childsnapshot.getValue().equals(isRecipeFavouriteQuery.getRecipeUid())) {
                                    Log.d("halo", "kurwa");
                                    asyncResponse.onSuccess(true);
                                    break;
                                }
                                asyncResponse.onSuccess(false);
                            }
                        } else
                            asyncResponse.onSuccess(false);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        asyncResponse.onException(databaseError.toException());
                    }
                });
    }
}

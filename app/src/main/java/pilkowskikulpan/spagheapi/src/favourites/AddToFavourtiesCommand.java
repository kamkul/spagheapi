package pilkowskikulpan.spagheapi.src.favourites;


import androidx.annotation.Nullable;

public class AddToFavourtiesCommand {
    @Nullable
    private String recipeUid;

    @Nullable
    public String getRecipeUid() {
        return recipeUid;
    }

    public void setRecipeUid(@Nullable String recipeUid) {
        this.recipeUid = recipeUid;
    }

    public boolean isValid() {
        return false;
    }


}

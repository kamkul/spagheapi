package pilkowskikulpan.spagheapi.src.favourites;

import dagger.Component;

@Component
public interface FavouritesComponent {
    FavouritesRepository getRepository();

    FavouritesService getService();
}

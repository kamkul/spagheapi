package pilkowskikulpan.spagheapi.src.favourites;

import java.util.ArrayList;

import javax.inject.Inject;

import pilkowskikulpan.spagheapi.src.recipe.Recipe;
import pilkowskikulpan.spagheapi.src.user.AsyncResponse;

public class FavouritesService {

    FavouritesRepository favouritesRepository;

    @Inject
    FavouritesService() {
        FavouritesComponent favourtiesComponent = DaggerFavouritesComponent.create();
        this.favouritesRepository = favourtiesComponent.getRepository();

    }

    public void addToFavourites(AddToFavourtiesCommand addToFavourtiesCommand,
                                AsyncResponse<Void> asyncResponse) {
        this.favouritesRepository.addToFavourites(addToFavourtiesCommand, asyncResponse);
    }

    public void deleteFromFavourites(DeleteFromFavouritesCommand deleteFromFavourites,
                                     AsyncResponse<Void> asyncResponse) {
        this.favouritesRepository.deleteFromFavourites(deleteFromFavourites, asyncResponse);
    }

    public void showYourFavourites(AsyncResponse<ArrayList<Recipe>> asyncResponse) {
        this.favouritesRepository.showYourFavourites(asyncResponse);
    }

    public void isFavourite(IsRecipeFavouriteQuery isRecipeFavouriteQuery, AsyncResponse<Boolean> asyncResponse) {
        this.favouritesRepository.isFavourite(isRecipeFavouriteQuery, asyncResponse);
    }
}
